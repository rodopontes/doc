# Documentação do Rodopontes


## Acesso

Você pode acessar o rodopontes através do endereço: https://app.rodopontes.com.br/

Ao acessar, será solicitado login e senha, conforme imagem à seguir:

![Tela de entrada](image/login.png)

Caso não tenha cadastro, é possível fazê-lo clicando no botão **Registre-se**, na próxima tela insira seus dados conforme imagem à seguir:


![Tela de entrada](image/registro.png)


## Listagem de pontes

Após autenticado, é possível ver as pontes inseridas através do aplicativo, conforme tela à seguir:

![Tela de entrada](image/pontes.png)

Nesta tela, é possível realizar filtragens por intervalo de datas, município e tipo do informante, conforme a seguinte tela:

![Tela de entrada](image/pontes_filtro.png)

Uma vez listadas as pontes, para cada registro, é possível detalhar, ver fotos e localizar geograficamente em um mapa.

Para detalhar os dados da ponte, clique no botão detalhar, conforme imagem à seguir:

![Tela de entrada](image/pontes_detalhe.png)

Para visualizar as fotos da região, clique no botão fotos, conforme ilustrado:

![Tela de entrada](image/ponteos_foto.png)

Finalmente, para visualizar a ponte num mapa clique no botão mapa, conforme a imagem:

![Tela de entrada](image/pontes_mapa_1.png)

Após clicar, verá o mapa

![Tela de entrada](image/potes_mapa_2.png)

Clicando no menu lateral **Mapa** será possível ver os registros dispostos em um mapa.

![Tela de entrada](image/pontes_mapa_3.png)

## Aplicativo

Acesso o aplicativo disponívem em: https://bit.ly/2FY2s8Y